##!/usr/bin/python
import firebase_admin
from firebase_admin import credentials, firestore
from datetime import datetime
from datetime import timedelta

UTC_OFFSET = 2 
pressure = 15000
temperature= 5000

datelocal = datetime.now()
date = datelocal - timedelta(hours = UTC_OFFSET)
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.000000000Z')

#************FIREBASE  CONFIG*******************************
cred = credentials.Certificate("./keyjson/opgcfinal-firebase-adminsdk-kr1bo-aa1b0b85f2.json")
#firebase_admin.initialize_app(cred)
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()
doc_ref = db.collection(u'firestore').document(u'android').collection(u'mesure').stream()
print("\n")
print("***********************************************")
print("*********Lecture dans firebase*****************")
#lecture firebase
for doc in doc_ref:
    print(u'{} => {}'.format(doc.id, doc.to_dict()))
print("***********************************************")
print("\n") 
print("***********************************************")
print("*********Ecriture dans firebase*****************")
data = {
    u'dateCreated': date,
    u'name': u'RASReact',
    u'pressure': pressure,
    u'temperature': temperature
}
print("\n") 
print("*************************************")
print("Envoi de la donnee au cloud")
# Add a new doc in collection 'cities' with ID 'LA'
db.collection(u'firestore').document(u'android').collection(u'mesure').document().set(data)
print("*************************************")
print("\n") 
from pyfcm import FCMNotification
push_service = FCMNotification(api_key="AAAADPeAh6U:APA91bGc288WAHZ79E24Dd8SFB-LwUHnqkpa2jtUL79n_peRIp72QESWelsDZgg3uEBsQUSsitsO8cSbrb_cnI3iUz72X_QPZug-2alXpUk1-LXaxDKSN5vFSgTRbB1aE2y_LSTbJRYb")
registration_id = "div0-gD9vkQ:APA91bHe6XAWxA2MGM97kdOi3uGn20mVTN_Pf46xB71-hYzyeYft5bCBfDBO3aqRtF35Rb3s4W1bpTAttzQ80Zhqq_SzOeYB-R92zkx0rva9nSCNQS0DPCSFRQELSXTSt67Z8t-TyHJU"
message_title = "MESURE"
message_body = "NOTIFICATION FROM PYTHON Alert!!!"
data_message = {
    "body" : "great match!"
}
print("\n") 
print("*************************************")
print("**************Notification***********")
print("Alerte Notification")
# To a single device
result = push_service.notify_single_device(registration_id=registration_id,message_title=message_title, message_body=message_body, data_message=data_message)
print("*************************************")
print("\n") 
