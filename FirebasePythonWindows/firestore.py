import sys
import datetime

import firebase_admin
from firebase_admin import credentials, firestore

date = datetime.datetime.now()
press = 20
temp = 10

cred = credentials.Certificate("./keyjson/opgcfinal-firebase-adminsdk-kr1bo-aa1b0b85f2.json")
#firebase_admin.initialize_app(cred)
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()
#REQ DE LECTURE LIT TOUTES LES DONNEES////
doc_ref = db.collection(u'firestore').document(u'android').collection(u'mesure').stream()
for doc in doc_ref:
    print(u'{} => {}'.format(doc.id, doc.to_dict()))

#REQ AVEC CONDITION WHERE////
docs = db.collection(u'firestore').document(u'android').collection(u'mesure').where(u'name', u'==', 'ESPx').stream()
for doc in docs:
    print(u'{} => {}'.format(doc.id, doc.to_dict()))

#ECRITURE DES DONNEES TO FIREBASE
data={
	u'dateCreated': date,
	u'name': u'PYTHONNERIR',
	u'pressure': press,
	u'temperature': temp
}

# Add a new doc in collection mesure (nouvelle mesure)
db.collection(u'firestore').document(u'android').collection(u'mesure').document().set(data)

#****************************************************************************************************

# response = getQuote()#Lfonction lecture de JSON
# dateCreated  = response.body['dateCreated']
# name = response.body['name']
# pressure = response.body['pressure']
# temperature = response.body['temperature']

# doc_ref = db.collection(u'firestore').document(u'android').collection(u'mesure')
# doc_ref.set ({
# 		u'dateCreated': dateCreated,
# 		u'name': name,
# 		u'pressure': pressure,
# 		u'temperature': temperature,
# 	})
# print(dateCreated+"and"+name+"and"+pressure+"and"+temperature+"successful written to database")

# try:
# 	doc = doc_ref.get()
# 	print(u'Document data: {}'.format(doc.to_dict()))
# 	#do somethong with the data
# except google.cloud.excetions.NotFound:
# 	print(u'No such document!')

#***********************************************************************#
#******************ENVOI DE NOTIFICATION CLIENT PYTHON******************#
#*******************************************pip install pyfscm**********#
#*************************https://github.com/olucurious/PyFCM***********#

from pyfcm import FCMNotification
#push_service = FCMNotification(api_key="<api-key>")
push_service = FCMNotification(api_key="AAAADPeAh6U:APA91bGc288WAHZ79E24Dd8SFB-LwUHnqkpa2jtUL79n_peRIp72QESWelsDZgg3uEBsQUSsitsO8cSbrb_cnI3iUz72X_QPZug-2alXpUk1-LXaxDKSN5vFSgTRbB1aE2y_LSTbJRYb")

registration_id = "div0-gD9vkQ:APA91bHe6XAWxA2MGM97kdOi3uGn20mVTN_Pf46xB71-hYzyeYft5bCBfDBO3aqRtF35Rb3s4W1bpTAttzQ80Zhqq_SzOeYB-R92zkx0rva9nSCNQS0DPCSFRQELSXTSt67Z8t-TyHJU"

message_title = "MESURE"
message_body = "NOTIFICATION FROM PYTHON"
# Sending a notification with data message payload
data_message = {
    "body" : "great match!"
}
# To a single device
result = push_service.notify_single_device(registration_id=registration_id,message_title=message_title, message_body=message_body, data_message=data_message)
